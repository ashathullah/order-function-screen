import { TestBed } from '@angular/core/testing';

import { GendralService } from './gendral.service';

describe('GendralService', () => {
  let service: GendralService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GendralService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
