import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultContentComponent } from './default-content/default-content.component';


const routes: Routes = [
  { path: '', component: DefaultContentComponent },
  { path: 'orderblock', loadChildren: () => import('./order/order.module').then(m => m.OrderModule) },
  { path: '', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }