import { Component } from '@angular/core';

@Component({
    selector: 'app-food-ready',
    templateUrl: './food-ready.component.html',
    styleUrls:['./food-ready.component.scss']
})

export class FoodReadyComponent {

    order={
        preparation_time: '15',
        delivery_partner: 'sarath'
    }
    
    bill = {
        total: '700',
        packing_charge: '10',
        tax: '48',
        discount: '0'
    }
    
}