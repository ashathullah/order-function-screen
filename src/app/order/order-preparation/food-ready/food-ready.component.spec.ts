import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodReadyComponent } from './food-ready.component';

describe('OrderComponent', () => {
  let component: FoodReadyComponent;
  let fixture: ComponentFixture<FoodReadyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodReadyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodReadyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
