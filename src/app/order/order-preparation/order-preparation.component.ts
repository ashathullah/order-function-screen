import { Component } from '@angular/core';

@Component({
    selector: 'app-order-preparation',
    templateUrl: './order-preparation.component.html',
    styleUrls: ['./order-preparation.component.scss']
})

export class OrderPreparationComponent {

    orders = [
        {
          id: "123456",
          time: "09:45 AM",
          type: "Delivery",
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        },
        {
          id: "123456",
          time: "09:45 AM",
          type: 'Self Service',
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        }
      ]

    activateFoodReady(id) {
      document.getElementById(id).classList.add("show");
    }

    deActivateFoodReady(id) {
      document.getElementById(id).classList.remove("show");
    }

    selectedIndex: number

    highlight(index: number) {
      this.selectedIndex = index;
    }

    constructor() {}
}