import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderComponent } from './order.component';
import { OrderRoutingModule } from './order-routing.module';
import { NewOrderComponent } from './new-order/new-order.component';
import { OrderTopNavComponent } from './order-top-nav/order-top-nav.component';
import { OrderPreparationComponent } from './order-preparation/order-preparation.component';
import { FoodReadyComponent } from './order-preparation/food-ready/food-ready.component';
import { OrderReadyComponent } from'../order/order-ready/order-ready.component';
import { HandoverComponent } from './order-ready/handover/handover.component';
import { OrderPickupComponent } from '../order/order-pickup/order-pickup.component';
import { PickedUpDetailsComponent } from './order-pickup/pickedup-details/pickedup-details.component';
import { CashOnPickUpComponent } from './cash-on-pickup/cash-on-pickup.component';
import { CashPaidDetailsComponent } from './cash-on-pickup/cash-paid-details/cash-paid-details.component';


@NgModule({
    imports: [
        CommonModule,
        OrderRoutingModule
    ],
    declarations: [
        OrderComponent,
        NewOrderComponent,
        OrderTopNavComponent,
        OrderPreparationComponent,
        FoodReadyComponent,
        OrderReadyComponent,
        HandoverComponent,
        OrderPickupComponent,
        PickedUpDetailsComponent,
        CashOnPickUpComponent,
        CashPaidDetailsComponent
    ],
    bootstrap: [OrderComponent]
})
export class OrderModule {

}