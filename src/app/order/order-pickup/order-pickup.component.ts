import { Component } from '@angular/core';

@Component({
    selector: 'app-order-pickup',
    templateUrl: "./order-pickup.component.html",
    styleUrls: ["./order-pickup.component.scss"]
})

export class OrderPickupComponent {
    
    orders = [
        {
          id: "123456",
          time: "09:45 AM",
          type: "Delivery",
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        },
        {
          id: "123456",
          time: "09:45 AM",
          type: 'dining',
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        }
      ]
  
      selectedIndex: number
  
      highlight(index: number) {
        this.selectedIndex = index;
      }

}