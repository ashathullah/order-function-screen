import { Component } from '@angular/core';

@Component({
    selector: 'app-pickedup-details',
    templateUrl: './pickedup-details.component.html',
    styleUrls:['./pickedup-details.component.scss']
})

export class PickedUpDetailsComponent {

    order={
        preparation_time: '15',
        delivery_partner: 'sarath'
    }
    
    bill = {
        total: '700',
        packing_charge: '10',
        tax: '48',
        discount: '0'
    }
    
}