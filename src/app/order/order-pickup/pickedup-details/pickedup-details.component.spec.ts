import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickedUpDetailsComponent } from './pickedup-details.component';

describe('OrderComponent', () => {
  let component: PickedUpDetailsComponent;
  let fixture: ComponentFixture<PickedUpDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickedUpDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickedUpDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
