import { Component } from '@angular/core';

@Component({
    selector: 'app-handover',
    templateUrl: './handover.component.html',
    styleUrls:['./handover.component.scss']
})

export class HandoverComponent {

    order={
        preparation_time: '15',
        delivery_partner: 'sarath'
    }
    
    bill = {
        total: '700',
        packing_charge: '10',
        tax: '48',
        discount: '0'
    }
    
}