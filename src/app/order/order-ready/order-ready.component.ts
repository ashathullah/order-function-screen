import { Component } from '@angular/core';

@Component({
    selector: 'app-order-ready',
    templateUrl: "./order-ready.component.html",
    styleUrls: ["./order-ready.component.scss"]
})

export class OrderReadyComponent {
    
    orders = [
        {
          id: "123456",
          time: "09:45 AM",
          type: "Delivery",
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        },
        {
          id: "123456",
          time: "09:45 AM",
          type: 'Self Service',
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        }
      ]

    activateHandover(id) {
        document.getElementById(id).classList.add("show");
      }
  
      deActivateHandover(id) {
        document.getElementById(id).classList.remove("show");
      }
  
      selectedIndex: number
  
      highlight(index: number) {
        this.selectedIndex = index;
      }

}