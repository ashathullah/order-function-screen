import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTopNavComponent } from './order-top-nav.component';

describe('OrderTopNavComponent', () => {
  let component: OrderTopNavComponent;
  let fixture: ComponentFixture<OrderTopNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTopNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTopNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
