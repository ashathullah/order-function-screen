import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-top-nav',
  templateUrl: './order-top-nav.component.html',
  styleUrls: ['./order-top-nav.component.scss']
})
export class OrderTopNavComponent implements OnInit {

  orderStates = [
    {
      lable: "New",
      quantity: 2,
      pagelink: 'newblock'
    },
    {
      lable: "Preparations",
      quantity: 0,
      pagelink: 'order-preparation'
    },
    {
      lable: "Ready",
      quantity: 1,
      pagelink: 'order-ready'
    },
    {
      lable: "Picked Up",
      quantity: 0,
      pagelink: 'order-pickup'
    },
    {
      lable: "Cash On Pick Up",
      quantity: 0,
      pagelink: 'cash-on-pickup'
    },
  ]

  constructor(public router: Router) { }

  ngOnInit(): void { }

  isActive(pageName) {
    const result = this.router.isActive('orderblock/' + pageName, false);
    return result;
  }

}
