import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { OrderComponent } from './order.component';
import { NewOrderComponent} from './new-order/new-order.component';
import { OrderPreparationComponent } from './order-preparation/order-preparation.component';
import { FoodReadyComponent } from './order-preparation/food-ready/food-ready.component';
import { OrderReadyComponent } from './order-ready/order-ready.component';
import { HandoverComponent } from './order-ready/handover/handover.component';
import { OrderPickupComponent } from '../order/order-pickup/order-pickup.component';
import { PickedUpDetailsComponent } from './order-pickup/pickedup-details/pickedup-details.component';
import { CashOnPickUpComponent } from './cash-on-pickup/cash-on-pickup.component';
import { CashPaidDetailsComponent } from './cash-on-pickup/cash-paid-details/cash-paid-details.component';

const routes: Routes = [
    { 
        path: '',
        component: OrderComponent,
        children: [
            { path: 'newblock', component: NewOrderComponent },
            { 
                path: 'order-preparation',
                component: OrderPreparationComponent,
                children: [
                    {
                        path: 'food-ready', component: FoodReadyComponent
                    }
                ]
            },
            { 
                path: 'order-ready',
                component: OrderReadyComponent,
                children: [
                    {
                        path: 'handover', component: HandoverComponent

                    }
                ]
            },
            {
                path: 'order-pickup',
                component: OrderPickupComponent,
                children: [
                    {
                        path: 'pickedup-details',
                        component: PickedUpDetailsComponent
                    }
                ]
            },
            {
                path: 'cash-on-pickup',
                component: CashOnPickUpComponent,
                children: [
                    {
                        path: 'cash-paid-details',
                        component: CashPaidDetailsComponent,
                    }
                ]
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class OrderRoutingModule { }