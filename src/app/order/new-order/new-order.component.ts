import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {
  orders = [
    {
      id: "123456",
      time: "09:45 AM",
      status: "Waiting for approvel",
      items : [
        {
          name: "Grill Chicken",
          unit: "1",
          quantity: "full",
          price: "300",
          addonUnit: "1",
          addonproduct: "kuboos",
          notes: 'Extra Spicy'
        },
        {
          name: "Grill Chicken",
          unit: "1",
          quantity: "full",
          price: "300",
          addonUnit: "1",
          addonproduct: "kuboos",
          notes: 'Extra Spicy'
        }
      ]
    },
    {
      id: "123456",
      time: "09:45 AM",
      status: "Waiting for approvel",
      items : [
        {
          name: "Grill Chicken",
          unit: "1",
          quantity: "full",
          price: "300",
          addonUnit: "1",
          addonproduct: "kuboos",
          notes: 'Extra Spicy'
        },
        {
          name: "Grill Chicken",
          unit: "1",
          quantity: "full",
          price: "300",
          addonUnit: "1",
          addonproduct: "kuboos",
          notes: 'Extra Spicy'
        }
      ]
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
