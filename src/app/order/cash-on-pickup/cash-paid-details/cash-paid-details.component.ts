import { Component } from '@angular/core';

@Component({
    selector: 'app-cash-paid-details',
    templateUrl: './cash-paid-details.component.html',
    styleUrls:['./cash-paid-details.component.scss']
})

export class CashPaidDetailsComponent {

    order={
        preparation_time: '15',
        delivery_partner: 'sarath'
    }
    
    bill = {
        total: '700',
        packing_charge: '10',
        tax: '48',
        discount: '0'
    }
    
}