import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashPaidDetailsComponent } from './cash-paid-details.component';

describe('OrderComponent', () => {
  let component: CashPaidDetailsComponent;
  let fixture: ComponentFixture<CashPaidDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashPaidDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashPaidDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
