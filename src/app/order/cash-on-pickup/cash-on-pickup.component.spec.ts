import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashOnPickUpComponent } from './cash-on-pickup.component';

describe('OrderComponent', () => {
  let component: CashOnPickUpComponent;
  let fixture: ComponentFixture<CashOnPickUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashOnPickUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashOnPickUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
