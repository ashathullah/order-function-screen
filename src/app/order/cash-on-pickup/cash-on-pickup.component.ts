import { Component } from '@angular/core';

@Component({
    selector: 'app-cash-on-pickup',
    templateUrl: './cash-on-pickup.component.html',
    styleUrls: ['./cash-on-pickup.component.scss']
})

export class CashOnPickUpComponent {
    
    orders = [
        {
          id: "123456",
          time: "09:45 AM",
          type: "Self pick up",
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        },
        {
          id: "123456",
          time: "09:45 AM",
          type: 'Self pick up',
          status: "Preparing",
          items : [
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            },
            {
              name: "Grill Chicken",
              unit: "1",
              quantity: "full",
              price: "300",
              addonUnit: "1",
              addonproduct: "kuboos",
              notes: 'Extra Spicy'
            }
          ]
        }
      ]
  
      selectedIndex: number
  
      highlight(index: number) {
        this.selectedIndex = index;
      }

}