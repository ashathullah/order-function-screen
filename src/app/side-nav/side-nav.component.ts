import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  navitems = [
    {
      lable:'Order',
      pagelink: '/orderblock'
    },
    {
      lable:'Billing',
      // pagelink: '#'
    },
    {
      lable:'Menu',
      // pagelink:'#'
    },
    {
      lable:'Promotions',
      // pagelink: '#'
    },
    {
      lable:'More...',
      // pagelink: '#'
    },
  ]

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  selectedIndex: number

  highliteActivation(target) {
    this.selectedIndex = target;  
  }
}
