import { Component, OnInit } from '@angular/core';
import { GendralService } from '../../assets/gendral.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  vendor = {
    name: 'Bhai Kadai',
    posting: 'cashier'
  }

  constructor(private service: GendralService) { }

  ngOnInit(): void {
  }

  

  captureOnOff() {
  }

}
